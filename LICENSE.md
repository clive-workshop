# clive licensing

- clive core code is under dual BSD3 and GPL3 license (main branch)
- clive documentation is under CC-BY-SA license (main branch)
- other branches may have different licenses
