#!/bin/bash
shutdown() {
#  echo "stop" | jack_transport
  exit 0
}
trap "shutdown" SIGINT SIGTERM
make -C client
make -C server API=sdl2
mkdir -p build
rm -f build/go*.so
SESSION="session-$(date -u +%F-%H%M%S)"
git checkout -b "${SESSION}"
(
#echo -e "stop\nlocate 0" | jack_transport
#ecasound -q -G:jack,record,send -f:f32,2,48000 -i:jack -o "${SESSION}.wav" &
#sleep 5
xterm -fa mono -fs 10 -geometry 80x10+0+0   -T server -e bash -c 'cd server ; while true ; do ./clive-server ; done' &
xterm -fa mono -fs 10 -geometry 80x30+0+228 -T client -e bash -c 'cd client ; while true ; do ./clive-client native-sse.mk ; done' &
xterm -fa mono -fs 10 -geometry 80x8+0+876  -T htop   -e bash -c 'while true ; do htop ; done' &
{ cd client ; while true ; do geany -mist dsp.h dsp/*.h go.c ; done }
wait
)
