#!/bin/bash
for c in $(cat /proc/cpuinfo | grep "^processor" | sed "s/processor\t: //")
do
  sudo cpufreq-set -c "${c}" -g performance
done
#xrandr --output LVDS1 --scale-from 1920x1080
