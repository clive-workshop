#!/bin/bash
REMOTEHOST="${1:-root@192.168.6.2}"
REMOTEPATH="${2:-Bela/projects/clive-core}"
#ssh-add
for c in $(cat /proc/cpuinfo | grep "^processor" | sed "s/processor\t: //")
do
  sudo cpufreq-set -c "${c}" -g performance
done
make -C client
mkdir -p build
rm -f build/go*.so
sshfs "${REMOTEHOST}:${REMOTEPATH}/build" ./build
SESSION="session-$(date -u +%F-%H%M%S)"
git checkout -b "${SESSION}"
(
#ssh "${REMOTEHOST}" "ecasound -q -G:jack,record,send -f:f32,2,48000 -i:jack -o '${REMOTEPATH}/${SESSION}.wav'" &
sleep 5
xterm -geometry 80x10+0+0   -T server -e ssh "${REMOTEHOST}" "${REMOTEPATH}/server/clive-server.sh" &
xterm -geometry 80x30+0+154 -T client -e bash -c 'cd client ; while true ; do ./clive-client bela.mk ; sleep 1 ; done' &
xterm -geometry 80x8+0+588  -T htop   -e bash -c 'while true ; do htop ; sleep 1 ; done' &
{ cd client ; while true ; do geany -mist dsp.h dsp/*.h go.c ; done } &
wait
)
ssh-add -d
