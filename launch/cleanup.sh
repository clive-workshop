#!/bin/bash
for c in $(cat /proc/cpuinfo | grep "^processor" | sed "s/processor\t: //")
do
  sudo cpufreq-set -c "${c}" -g ondemand
done
xrandr --auto
