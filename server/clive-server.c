#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#ifdef __x86_64__
#include <fenv.h>
#endif

#include <linux/limits.h>
#include <sys/inotify.h>
#include <dlfcn.h>

#if defined(BELA) + defined(JACK) + defined(SDL2) != 1
#error exactly one audio API must be defined
#endif

#ifdef BELA
#include <stdarg.h>
#include <Bela.h>
#endif

#ifdef JACK
#include <jack/jack.h>
#endif

#ifdef SDL2
#include <SDL2/SDL.h>
#include <SDL2/SDL_audio.h>
#endif

// TODO: make it settable as a command line argument
#define INCHANNELS 2
#define OUTMAIN 2
#ifdef JACK
#define OUTSCOPE 16
#else
#define OUTSCOPE 0
#endif
#define OUTCHANNELS (OUTMAIN + OUTSCOPE)

// per-sample callback implemented in go.so
typedef int callback(void *, int, const float *, int, float *);

// default silent callback
static int deffunc(void *data, int inchannels, const float *in, int outchannels, float *out) {
  (void) data;
  (void) in;
  for (int c = 0; c < outchannels; ++c) {
    out[c] = 0;
  }
  return 0;
}

static struct {
#ifdef BELA
#endif
#ifdef JACK
  jack_client_t *client;
  jack_port_t *in[INCHANNELS], *out[OUTCHANNELS];
#endif
  void *data;
  callback * volatile func;
  int volatile reload;
} state;

// race mitigation
volatile int inprocesscb = 0;

volatile int running = 1;
void interrupt_handler(int var)
{
  (void) var;
  running = 0;
}

#ifdef BELA
bool setup(BelaContext *context, void *userData)
{
  (void) context;
  (void) userData;
  return true;
}

void cleanup(BelaContext *context, void *userData)
{
  (void) context;
  (void) userData;
}

void render(BelaContext *context, void *userData)
{
  (void) userData;
  inprocesscb = 1; // race mitigation
  // get callback
  callback *f = state.func;
  // handle reloading
  if (state.reload) {
    int *reloaded = state.data;
    *reloaded = 1;
    state.reload = 0;
  }
  // loop over samples
  for (unsigned int n = 0; n < context->audioFrames; n++)
  {
    // to buffer
    float in[INCHANNELS];
    float out[OUTCHANNELS];
    for (unsigned int channel = 0; channel < INCHANNELS; channel++)
    {
      if (channel < context->audioInChannels)
        in[channel] = audioRead(context, n, channel);
      else
        in[channel] = 0;
    }
    for (unsigned int channel = 0; channel < context->audioOutChannels; channel++)
    {
      out[channel] = 0;
    }
    // callback
    f(state.data, INCHANNELS, in, OUTCHANNELS, out);
    // from buffer
    for (unsigned int channel = 0; channel < OUTCHANNELS; channel++)
    {
      if (channel < context->audioOutChannels)
        audioWrite(context, n, channel, out[channel]);
    }
  }
  // done
  inprocesscb = 0; // race mitigation
}
#endif

#ifdef JACK
static int processcb(jack_nframes_t nframes, void *arg) {
  inprocesscb = 1; // race mitigation
  // set floating point environment for denormal->0.0
#ifdef __x86_64__
  fenv_t fe;
  fegetenv(&fe);
  unsigned int old_mxcsr = fe.__mxcsr;
  fe.__mxcsr |= 0x8040; // set DAZ and FTZ
  fesetenv(&fe);
#endif
  // get jack buffers
  jack_default_audio_sample_t *in [INCHANNELS];
  jack_default_audio_sample_t *out[OUTCHANNELS];
  for (int c = 0; c < INCHANNELS; ++c) {
    in [c] = (jack_default_audio_sample_t *) jack_port_get_buffer(state.in [c], nframes);
  }
  for (int c = 0; c < OUTCHANNELS; ++c) {
    out[c] = (jack_default_audio_sample_t *) jack_port_get_buffer(state.out[c], nframes);
  }
  // clear out to prevent possibly loud xruns
  for (int c = 0; c < OUTCHANNELS; ++c) {
    for (jack_nframes_t i = 0; i < nframes; ++i) {
      out[c][i] = 0;
    }
  }
  // get callback
  callback *f = state.func;
  // handle reloading
  if (state.reload) {
    int *reloaded = state.data;
    *reloaded = 1;
    state.reload = 0;
  }
  // loop over samples
  for (jack_nframes_t i = 0; i < nframes; ++i) {
    // to buffer
    float ini[INCHANNELS];
    float outi[OUTCHANNELS];
    for (int c = 0; c < INCHANNELS; ++c) {
      ini [c] = in[c][i];
    }
    for (int c = 0; c < OUTCHANNELS; ++c) {
      outi[c] = 0;
    }
    // callback
    /* int inhibit_reload = */ f(state.data, INCHANNELS, ini, OUTCHANNELS, outi);
    // from buffer
    for (int c = 0; c < OUTCHANNELS; ++c) {
      out[c][i] = outi[c];
    }
  }
  // restore floating point environment
#ifdef __x86_64__
  fe.__mxcsr = old_mxcsr;
  fesetenv(&fe);
#endif
  // done
  inprocesscb = 0; // race mitigation
  return 0;
}

static void errorcb(const char *desc) {
  fprintf(stderr, "JACK error: %s\n", desc);
}

static void shutdowncb(void *arg) {
  exit(1);
}

static int sampleratecb(jack_nframes_t sr, void *arg) {
  fprintf(stderr, "JACK sample rate: %d\n", sr);
  return 0;
}

static void atexitcb(void) {
  jack_client_close(state.client);
}
#endif

#ifdef SDL2
static void audiocb(void *userdata, Uint8 *stream, int len) {
  inprocesscb = 1; // race mitigation
  // set floating point environment for denormal->0.0
#ifdef __x86_64__
  fenv_t fe;
  fegetenv(&fe);
  unsigned int old_mxcsr = fe.__mxcsr;
  fe.__mxcsr |= 0x8040; // set DAZ and FTZ
  fesetenv(&fe);
#endif
  // get buffers
  float *out = (float *) stream;
  int nframes = len / sizeof(float) / OUTCHANNELS;
  // get callback
  callback *f = state.func;
  // handle reloading
  if (state.reload) {
    int *reloaded = state.data;
    *reloaded = 1;
    state.reload = 0;
  }
  // loop over samples
  int k = 0;
  for (int i = 0; i < nframes; ++i) {
    // to buffer
    float ini[INCHANNELS];
    float outi[OUTCHANNELS];
    for (int c = 0; c < INCHANNELS; ++c) {
      ini [c] = 0;
    }
    for (int c = 0; c < OUTCHANNELS; ++c) {
      outi[c] = 0;
    }
    // callback
    /* int inhibit_reload = */ f(state.data, INCHANNELS, ini, OUTCHANNELS, outi);
    // from buffer
    for (int c = 0; c < OUTCHANNELS; ++c) {
      out[k++] = outi[c];
    }
  }
  // restore floating point environment
#ifdef __x86_64__
  fe.__mxcsr = old_mxcsr;
  fesetenv(&fe);
#endif
  // done
  inprocesscb = 0; // race mitigation
}
#endif

int main(int argc, char **argv) {
  srand(time(0));
  state.func = deffunc;
  state.data = calloc(1, 64 * 1024 * 1024);
  state.reload = 0;
#ifdef BELA
  BelaInitSettings *settings = Bela_InitSettings_alloc();
  Bela_defaultSettings(settings);
  settings->setup = setup;
  settings->render = render;
  settings->cleanup = cleanup;
  if (Bela_initAudio(settings, 0))
  {
    Bela_InitSettings_free(settings);
    fprintf(stderr, "could not init audio\n");
    return 1;
  }
  Bela_InitSettings_free(settings);
  if (Bela_startAudio())
  {
    fprintf(stderr, "could not start audio\n");
    return 1;
  }
#endif
#ifdef JACK
  jack_set_error_function(errorcb);
  if (!(state.client = jack_client_open("clive", JackNoStartServer, 0))) {
    fprintf(stderr, "jack server not running?\n");
    return 1;
  }
  atexit(atexitcb);
  jack_set_process_callback(state.client, processcb, 0);
  jack_on_shutdown(state.client, shutdowncb, 0);
  jack_set_sample_rate_callback(state.client, sampleratecb, 0);
  // multi-channel processing
  for (int c = 0; c < INCHANNELS; ++c) {
    char portname[100];
    snprintf(portname, 100, "input_%d",  c + 1);
    state.in[c]  = jack_port_register(state.client, portname, JACK_DEFAULT_AUDIO_TYPE, JackPortIsInput,  0);
  }
  for (int c = 0; c < OUTCHANNELS; ++c) {
    char portname[100];
    snprintf(portname, 100, "output_%d", c + 1);
    state.out[c] = jack_port_register(state.client, portname, JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0);
  }
  if (jack_activate(state.client)) {
    fprintf (stderr, "cannot activate JACK client");
    return 1;
  }
  // stereo recording
  jack_connect(state.client, "clive:output_1", "record:in_1");
  jack_connect(state.client, "clive:output_2", "record:in_2");
  // stereo output to system
  const char **ports;
  if ((ports = jack_get_ports(state.client, NULL, NULL, JackPortIsPhysical | JackPortIsInput)))
  {
    int i = 0;
    while (ports[i] && i < 2)
    {
      jack_connect(state.client, jack_port_name(state.out[i]), ports[i]);
      i++;
    }
    free(ports);
  }
  // stereo input from system
  if ((ports = jack_get_ports(state.client, NULL, NULL, JackPortIsPhysical | JackPortIsOutput)))
  {
    int i = 0;
    while (ports[i] && i < 2)
    {
      jack_connect(state.client, ports[i], jack_port_name(state.in[i]));
      i++;
    }
    free(ports);
  }
  // stereo output to pulse
  jack_connect(state.client, "clive:output_1", "PulseAudio JACK Source:front-left");
  jack_connect(state.client, "clive:output_2", "PulseAudio JACK Source:front-right");
  // stereo input from pulse
  jack_connect(state.client, "PulseAudio JACK Sink:front-left",  "clive:input_1");
  jack_connect(state.client, "PulseAudio JACK Sink:front-right", "clive:input_2");
  // multichannel output to exwhyscope
  jack_connect(state.client, "clive:output_3", "exwhyscope:in_1_x");
  jack_connect(state.client, "clive:output_4", "exwhyscope:in_1_y");
  jack_connect(state.client, "clive:output_5", "exwhyscope:in_2_x");
  jack_connect(state.client, "clive:output_6", "exwhyscope:in_2_y");
  jack_connect(state.client, "clive:output_7", "exwhyscope:in_3_x");
  jack_connect(state.client, "clive:output_8", "exwhyscope:in_3_y");
  jack_connect(state.client, "clive:output_9", "exwhyscope:in_4_x");
  jack_connect(state.client, "clive:output_10", "exwhyscope:in_4_y");
  jack_connect(state.client, "clive:output_11", "exwhyscope:in_5_x");
  jack_connect(state.client, "clive:output_12", "exwhyscope:in_5_y");
  jack_connect(state.client, "clive:output_13", "exwhyscope:in_6_x");
  jack_connect(state.client, "clive:output_14", "exwhyscope:in_6_y");
  jack_connect(state.client, "clive:output_15", "exwhyscope:in_7_x");
  jack_connect(state.client, "clive:output_16", "exwhyscope:in_7_y");
  jack_connect(state.client, "clive:output_17", "exwhyscope:in_8_x");
  jack_connect(state.client, "clive:output_18", "exwhyscope:in_8_y");
#endif
#ifdef SDL2
  // initialize SDL2 audio
  SDL_Init(SDL_INIT_AUDIO);
  SDL_AudioSpec want, have;
  want.freq = 48000; // FIXME don't hardcode desired sample rate
  want.format = AUDIO_F32;
  want.channels = OUTCHANNELS;
  want.samples = 4096; // FIXME don't hardcode buffer size
  want.callback = audiocb;
  SDL_AudioDeviceID dev = SDL_OpenAudioDevice(NULL, 0, &want, &have, SDL_AUDIO_ALLOW_ANY_CHANGE);
  if (have.format != AUDIO_F32 || have.channels != OUTCHANNELS)
  {
    fprintf(stderr, "want: %d %d %d %d\n", want.freq, want.format, want.channels, want.samples);
    fprintf(stderr, "have: %d %d %d %d\n", have.freq, have.format, have.channels, have.samples);
    fprintf(stderr, "error: bad audio parameters\n");
    return 1;
  }
  SDL_PauseAudioDevice(dev, 0);
#endif
  signal(SIGINT, interrupt_handler);
  signal(SIGTERM, interrupt_handler);
  // watch for filesystem changes
  int ino = inotify_init();
  if (ino == -1) {
    perror("inotify_init()");
    return 1;
  }
  int wd = inotify_add_watch(ino, "../build/", IN_CLOSE_WRITE);
  if (wd == -1) {
    perror("inotify_add_watch()");
    return 1;
  }
  ssize_t buf_bytes = sizeof(struct inotify_event) + NAME_MAX + 1;
  char *buf = malloc(buf_bytes);
  // double-buffering stuff
  void *old_dl = 0;
  void *new_dl = 0;
  int which = 0;
  // main loop
  while (running) {
/*
Double buffering to avoid glitches on reload.  Can't dlopen the same file
twice (or even symlinks thereof) due to aggressive caching seemingly based
on file names by libdl, so copy the target to one of two names.
To avoid crashing by unloading running code, open the other .so filename to the
runnning code before swapping pointers when hopefully not in a JACK process
callback in the DSP thread.  Finally dlclose the previous and swap the buffer
index.
*/
    const char *copycmd[2] =
      { "cp -f ../build/go.so ../build/go.a.so"
      , "cp -f ../build/go.so ../build/go.b.so"
      };
    const char *library[2] =
      { "../build/go.a.so"
      , "../build/go.b.so"
      };
    if (system(copycmd[which])) {
      fprintf(stderr, "\x1b[31;1mCOPY COMMAND FAILED: '%s'\x1b[0m\n", copycmd[which]);
    }
    else if ((new_dl = dlopen(library[which], RTLD_NOW))) {
      callback *new_cb;
      *(void **) (&new_cb) = dlsym(new_dl, "go");
      if (new_cb) {
        // race mitigation: dlclose with jack running in .so -> boom
        while (inprocesscb) ;
        // JACK process callback not running
        state.func = new_cb;
        state.reload = 1;
        // JACK process callback might have started again
        // race mitigation
        while (inprocesscb) ;
        // JACK process callback not running, and not referencing old state.func
        if (old_dl) {
          dlclose(old_dl);
        }
        old_dl = new_dl;
        new_dl = 0;
        which = 1 - which;
        fprintf(stderr, "\x1b[32;1mRELOADED: '%p'\x1b[0m\n", *(void **) (&new_cb));
      } else {
        fprintf(stderr, "\x1b[31;1mNO FUNCTION DEFINED: 'go()'\x1b[0m\n");
        dlclose(new_dl);
      }
    } else {
      // another race condition: the .so disappeared before load
      fprintf(stderr, "\x1b[31;1mFILE VANISHED: '%s'\x1b[0m\n", library[which]);
      const char *err = dlerror();
      if (err) {
        fprintf(stderr, "\x1b[31;1m%s\x1b[0m\n", err);
      }
    }
    // read events (blocking)
    int done = 0;
    do {
      memset(buf, 0, buf_bytes);
      ssize_t r = read(ino, buf, buf_bytes);
      if (r == -1) {
        perror("read()");
        sleep(1);
      } else {
        char *bufp = buf;
        while (bufp < buf + r)
        {
          struct inotify_event *ev = (struct inotify_event *) bufp;
          bufp += sizeof(struct inotify_event) + ev->len;
          if (ev->mask & IN_CLOSE_WRITE) {
            fprintf(stderr, "\x1b[32;1mFILE CHANGED: '%s'\x1b[0m\n", ev->name);
            if (0 == strcmp("go.so", ev->name)) {
              done = 1;
            }
          }
        }
      }
    } while (running && ! done);
  }
#ifdef BELA
  Bela_stopAudio();
  Bela_cleanupAudio();
#endif
#ifdef JACK
#endif
  // never reached
  close(ino);
  return 0;
}
