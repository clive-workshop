typedef float sample;
#define SR 48000
#include "dsp.h"

typedef struct {
  int reloaded;

} S;

int go(S *s, int inchannels, const float *in, int outchannels, float *out) {
  if (s->reloaded) {
    s->reloaded = 0;
  }
  for (int c = 0; c < outchannels; ++c) {
    out[c] = 0;
  }
  return 0;
}
