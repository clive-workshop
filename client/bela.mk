GCC ?= arm-linux-gnueabihf-gcc-6

../build/go.so: go.c dsp.h dsp/*.h
	$(GCC) -std=c99 -Wall -pedantic -Wextra -Wno-unused-parameter -O3 -march=armv7-a -mtune=cortex-a8 -mfloat-abi=hard -mfpu=neon -ftree-vectorize -I. -shared -fPIC -o ../build/go.so go.c -lm
