#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include <linux/limits.h>
#include <sys/inotify.h>

int main(int argc, char **argv) {
  // build remake command line
  // FIXME this doesn't quote or escape or otherwise protect argv[1]
  // FIXME don't pass untrusted input to this command!
  const char *templat =
    "git add go.c ; "
    "git --no-pager diff --cached --color ; "
    "git commit -uno -m \"go $(date --iso=s)\" ; "
    "make --quiet -f "
    ;
  const char *makefile = "native-sse.mk";
  if (argc > 1)
  {
    makefile = argv[1];
  }
  size_t bytes = strlen(templat) + strlen(makefile) + 1;
  char *command = malloc(bytes);
  if (! command) {
    perror("malloc()");
    return 1;
  }
  command[0] = 0;
  strncat(command, templat, bytes - 1 - strlen(command));
  strncat(command, makefile, bytes - 1 - strlen(command));
  // watch for filesystem changes
  int ino = inotify_init();
  if (ino == -1) {
    perror("inotify_init()");
    return 1;
  }
  int wd = inotify_add_watch(ino, ".", IN_CLOSE_WRITE);
  if (wd == -1) {
    perror("inotify_add_watch()");
    return 1;
  }
  ssize_t buf_bytes = sizeof(struct inotify_event) + NAME_MAX + 1;
  char *buf = malloc(buf_bytes);
  if (! buf)
  {
    perror("malloc()");
    return 1;
  }
  // recompile
  int ret = system(command);
  if (ret == -1 || ! WIFEXITED(ret) || WEXITSTATUS(ret) != 0) {
    fprintf(stderr, "\x1b[31;1m%s: %d\x1b[0m\n", "SYSTEM ERROR", ret);
  }
  // main loop
  while (1) {
    // read events (blocking)
    memset(buf, 0, buf_bytes);
    ssize_t r = read(ino, buf, buf_bytes);
    if (r == -1) {
      perror("read()");
      sleep(1);
    } else {
      char *bufp = buf;
      while (bufp < buf + r)
      {
        struct inotify_event *ev = (struct inotify_event *) bufp;
        bufp += sizeof(struct inotify_event) + ev->len;
        if (ev->mask & IN_CLOSE_WRITE) {
          fprintf(stderr, "\x1b[32;1mFILE CHANGED: '%s'\x1b[0m\n", ev->name);
          if (0 == strcmp("go.c", ev->name)) {
            // recompile
            int ret = system(command);
            if (ret == -1 || ! WIFEXITED(ret) || WEXITSTATUS(ret) != 0) {
              fprintf(stderr, "\x1b[31;1m%s: %d\x1b[0m\n", "SYSTEM ERROR", ret);
            }
          }
        }
      }
    }
  }
  // never reached
  close(ino);
  free(command);
  return 0;
}
