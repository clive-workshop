#ifndef DSP_FUNC_H
#define DSP_FUNC_H 1

#include <tgmath.h>
#include <stdbool.h>
#include <stdlib.h>

#define   likely(x) __builtin_expect((x),1)
#define unlikely(x) __builtin_expect((x),0)

#define pi 3.141592653589793
#define twopi 6.283185307179586

static inline sample wrap(sample x) {
  return x - floor(x);
}

static inline sample wrapto(sample x, sample n) {
  return wrap(x / n) * n;
}

static inline sample wrapat(sample x, sample n) {
  return wrap(x * n) / n;
}

static inline sample clamp(sample x, sample lo, sample hi) {
  return fmin(fmax(x, lo), hi);
}

static inline sample mix(sample x, sample y, sample t) {
  return (1 - t) * x + t * y;
}

static inline sample trisaw(sample x, sample t) {
  sample s = clamp(t, (sample)0.0000001, (sample)0.9999999);
  sample y = clamp(x, 0, 1);
  return y < s ? y / s : (1 - y) / (1 - s);
}

static inline sample noise() {
  return 2 * (rand() / (sample) RAND_MAX - (sample)0.5);
}

static inline sample ABS(sample x)
{
  return x < 0 ? -x : x;
}

static inline sample CLAMP(sample x, sample lo, sample hi)
{
  return lo < x ? x < hi ? x : hi : lo;
}

static inline sample WRAP(sample x)
{
  return x - floor(x);
}

static inline sample FMOD(sample x, sample y)
{
  return WRAP(x / y) * y;
}

static const double PI   = 3.141592653589793;
static const double PI_2 = 1.5707963267948966;
static const double PI_4 = 0.7853981633974483;

static inline double TAN(double x)
{
  bool recip = false;
  x = ABS(x);
  if (unlikely(x >= PI_2))
  {
    x = FMOD(x + PI_2, PI) - PI_2;
  }
  if (unlikely(x >= PI_4))
  {
    recip = true;
    x = PI_2 - x;
  }
  // 0 <= x <= pi/4
  // [5/6] Padé approximant
  double x2 = x * x;
  double a = x * (10395.0 + x2 * (-1260.0 + x2 * 21.0));
  double b = 10395.0 + x2 * (-4725.0 + x2 * (210.0 - x2));
  return recip ? b / a : a / b;
}

static inline double TANH(double x)
{
//  x = CLAMP(x, -4.0, 4.0);
  // [5/6] Padé approximant
  double x2 = x * x;
  double a = x * (10395.0 + x2 * (1260.0 + x2 * 21.0));
  double b = 10395.0 + x2 * (4725.0 + x2 * (210.0 + x2));
  return a / b;
}

// pd-0.45-5/src/d_math.c

#define log10overten 0.23025850929940458
#define tenoverlog10 4.3429448190325175

static inline sample mtof(sample f) {
  return (sample)8.17579891564 * exp((sample)0.0577622650 * fmin(f, 1499));
}

static inline sample ftom(sample f) {
  return (sample)17.3123405046 * log((sample)0.12231220585 * f);
}

static inline sample dbtorms(sample f) {
  return exp((sample)0.5 * (sample)log10overten * (fmin(f, 870) - 100));
}

static inline sample rmstodb(sample f) {
  return 100 + 2 * (sample)tenoverlog10 * log(f);
}

static inline sample dbtopow(sample f) {
  return exp((sample)log10overten * (fmin(f, 485) - 100));
}

static inline sample powtodb(sample f) {
  return 100 + (sample)tenoverlog10 * log(f);
}


static inline sample bytebeat(int x) {
  return ((x & 0xFF) - 0x80) / (sample) 0x80;
}

static inline sample bitcrush(sample x, sample bits) {
  sample n = pow(2, bits);
  return round(x * n) / n;
}

#endif
