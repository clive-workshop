GCC ?= gcc

../build/go.so: go.c dsp.h dsp/*.h
	$(GCC) -std=c99 -Wall -pedantic -Wextra -Wno-unused-parameter -O3 -march=native -mfpmath=sse -I. -shared -fPIC -o ../build/go.so go.c -lm
