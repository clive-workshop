#!/bin/sh
git log --pretty=format:"%H" --reverse --after 2012-01-01 |
while read commit
do
  git show --color --word-diff=color --pretty=format:"%n%Cblue%H%Creset %C(yellow)%ci%Creset%n" $commit
done |
most
