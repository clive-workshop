# clive

C audio live-coding skeleton
<https://mathr.co.uk/clive>.

## audio api support

- jack (recommended, default, with scripts for recording)
- bela (for the single board computer)
- sdl2 (playback only, works with pulseaudio)

## deps

  - linux (POSIX for `-ldl`, Linux for `inotify`)

### server

  - make
  - gcc
  - git
  - jack-dev
  - libsdl2-dev
  - pkg-config
  - ecasound

### client

  - make
  - gcc
  - git
  - geany
  - htop
  - xterm

## usage

Editor save -> client recompiles code -> server reloads binary.

Automatic recording of code edits to git history.

Automatic recording of audio output to WAV file.

### local

Run editor, client, server all on one machine.  Assumes x86(_64) with SSE:

    git clone https://code.mathr.co.uk/clive-core.git
    cd clive-core
    ./launch/local-native-sse.sh

### remote

Run editor and client on local machine, server on a remote machine.

#### rpi

Assumes Raspberry Pi Model 3 B remote running Debian Buster aarch64 (arm64)
from unofficial unsupported image at <https://wiki.debian.org/RaspberryPi3>.

Client machine needs `gcc-8-aarch64-linux-gnu` installed.

Client user needs an ssh key.

Server machine needs `~/.ssh/authorized_keys` for the client user.

Server machine needs working JACK audio server; for some tips see:
<https://mathr.co.uk/blog/2018-10-23_jack_on_top_of_pulseaudio.html>
Alternatively, compiling the clive server with `make API=sdl2` might allow
audio via Pulseaudio to work more directly, but recording via `ecasound`
won't work out of the box.  To be tested and documented properly.

Run once on server and client:

    mkdir -p ~/code
    cd ~/code
    git clone https://code.mathr.co.uk/clive-core.git

Run each session on client:

    cd ~/code/clive-core
    ./launch/remote-rpi3b.sh user@server code/clive-core

Note: cleanup is not yet automatic; you need to kill left-over tasks on
the server host manually.  `ps aux | grep clive` may offer some hints.

#### bela

For Bela running its system (`armv7l`) connected via USB to a Debian
machine.  No ssh keys needed as passwordless root works by default.

Server machine (the Bela) needs directory layout like
`/root/Bela/projects/clive-core`.  You need the `bela` branch of the
repository.  Compile `clive-server` on the Bela with `make API=bela`.
Launch it by `cd server; ./clive-server`.
The script `launch/remote-bela.sh` does this automatically (and more),
but read it first in case it does not match your setup.

Client machine needs to be Debian Stretch (native or a chroot).  You
can install a chroot with `debootstrap`, read its fine documentation.
Debian Buster won't work because its `glibc/libm` is too new for the
Bela, giving this error message when `clive-server` attempts to load
the cross-compiled code:

    /lib/arm-linux-gnueabihf/libm.so.6: version 'GLIBC_2.27' not found

Client machine needs `sshfs` installed.  You might need to create the
`/dev/fuse` device node inside the chroot if you are using one:

    mknod /dev/fuse c 10 229
    chmod g+w,o+w /dev/fuse

Client machine needs `gcc-6-arm-linux-gnueabihf` cross-compiler
installed.  If you get errors relating to `crti.o` or `-lm` or other
basic functionality, you might need to install more packages, not sure
which but I installed `g++-6-arm-linux-gnueabihf` and it worked for me.

Client machine chroot user numeric ids should match your regular user,
if you have a single user then both should be 1000.  This allows you to
run a graphical-mode text editor outside the chroot, modifying files
inside it with the correct permissions.

If using a chroot, you need to `sshfs` mount the Bela `clive-core/build`
directory over the `clive-core/build` directory in the chroot.  If using
a chroot, `./clive-client bela.mk` should be run inside it.  If using a
native client host, you can use `launch/remote-bela.sh` which does
both of these things (and more).

Note: cleanup is not yet automatic; you need to kill left-over tasks on
the Bela manually.  `ps aux | grep clive` may offer some hints.

Note: there is no audio recording support for Bela.

## html export

HTML5 with audio player synced to syntax-highlighted source code diffs:

    ./extra/session2html.sh session-branch [start-datetime]

Expects the session audio recording encoded to `.ogg`.

Adjust `start-datetime` if the audio is out of sync (most commonly by an
hour in summer due to daylight saving time).

webvtt would be better than Javascript searching a dictionary on each
time change, if only it worked...

## timebase idea

some kind of encoding of timestamps as audio, so cut/paste editing of
sessions can still be linked to code changes (ie, at each cut boundary
get a combined diff of all the changes that were skipped)
